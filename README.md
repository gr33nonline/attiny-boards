# ATtiny Boards

A collection of schematics of ATtiny development boards

See:

 - [Digistumped?](https://gr33nonline.wordpress.com/2019/08/09/digistump/)
 - [Using an ATtiny development board](https://gr33nonline.wordpress.com/2019/05/08/using-a-attiny-development-board/)
 - [A tiny one](https://gr33nonline.wordpress.com/2019/03/13/a-tiny-one/)